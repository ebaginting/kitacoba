from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import User

# Create your views here.

def index(request):
    count = User.objects.count()
    return render(request, "story9/index.html",{
        'count' : count
    })

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            return redirect('/story9accounts/login/')
    else :
        form = UserCreationForm()
    return render(request,"registration/signup.html", {
        'form' : form
    })