from django.urls import path, include
from django.contrib import admin
from . import views

app_name = 'story9'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('signup/', views.signup, name="signup"),
    path('accounts/', include('django.contrib.auth.urls')),
]