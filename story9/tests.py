from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

# Create your tests here.
class Story9TestCase(TestCase):
    def test_url_is_exist(self):
        response = Client().get("/story9")
        self.assertEqual(response.status_code, 200)

    def test_is_template_used(self):
        response = Client().get("/story9")
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_login_response(self):
        response = Client().get("/story9accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_login_template(self):
        response = Client().get("/story9accounts/login/")
        self.assertTemplateUsed(response, "registration/login.html")
    
    def test_signup_response(self):
        response = Client().get("/story9signup/")
        self.assertEqual(response.status_code, 200)
    
    def test_signup_template(self):
        response = Client().get("/story9signup/")
        self.assertTemplateUsed(response, "registration/signup.html")

