from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views
# Create your tests here.

class Story7TestCase(TestCase):
    def test_url_is_exist(self):
        response = Client().get("/story7")
        self.assertEqual(response.status_code, 200)

    def test_is_template_used(self):
        response = Client().get("/story7")
        self.assertTemplateUsed(response, 'story7/index.html')

    def test_views(self):
        found = resolve("/story7")            
        self.assertEqual(found.func, views.index)