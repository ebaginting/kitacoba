from django.urls import path
from django.contrib import admin
from . import views

app_name = 'story6'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('tambah/', views.add_event, name='add_event'),
    path('tambah_orang/', views.add_orang, name='add_orang'), 
]