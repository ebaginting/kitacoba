from django.db import models

# Create your models here.
class Acara(models.Model):
    nama_acara = models.CharField(max_length = 50)

    def __str__(self):
        return self.nama_acara

class Peserta(models.Model):
    nama = models.CharField(max_length = 30)
    acara = models.ForeignKey(Acara, on_delete=models.CASCADE, related_name = 'attendances')

    def __str__(self):
        return self.nama