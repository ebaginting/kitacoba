from django.shortcuts import render,redirect
from .forms import FormAcara, FormPeserta
from .models import Acara,Peserta

# Create your views here.
def index(request) :
    acara = Acara.objects.all()
    peserta = Peserta.objects.filter(acara=acara)
    return render(request, "story6/index.html", {
        'acara' : acara,
        'peserta' : peserta,
    })


def add_event(request) :
    if request.method == 'POST' :
        form_satu = FormAcara(request.POST)
        if form_satu.is_valid():
            form_satu.save()
            return redirect('story6:index')

    else :
        form_satu = FormAcara()
    return render(request, 'story6/add_acara.html', {
        'form_satu' : form_satu
    })
    
def add_orang(request) :
    if request.method == 'POST' :
        form_dua = FormPeserta(request.POST)
        if form_dua.is_valid():
            form_dua.save()
            return redirect('story6:index')
    else :
        form_dua = FormPeserta()
    return render(request, 'story6/add_orang.html',{
        'form_dua' : form_dua
    })