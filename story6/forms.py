from django import forms
from .models import Acara,Peserta

class FormAcara(forms.ModelForm):
    class Meta :
        model = Acara
        fields = ('nama_acara',)

class FormPeserta(forms.ModelForm) :
    class Meta :
        model = Peserta
        fields = ('nama',)