from django.db import models

# Create your models here.

class gForm(models.Model) :
    term = models.CharField(max_length=120) #max_length = required
    subject = models.CharField(max_length=120) #max_length = required
    credit = models.CharField(max_length=120) #max_length = required
    lecturer = models.CharField(max_length=120) #max_length = required
    classroom = models.CharField(max_length=120) #max_length = required
    description = models.CharField(max_length=240) #max_length = required
    featured = models.BooleanField(default=True)
    
def __str__(self):
    return self.subject
    