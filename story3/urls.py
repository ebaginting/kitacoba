from django.urls import path

from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.index, name='index'),
    path('newindex', views.about, name="about"),
    path('index.html', views.contacts, name='contacts'),
    path('isi_form',views.isi_form, name="isi_form"),
    path('lihat_response',views.lihat_response, name="lihat_response"),
    path('delete/<int:id>', views.delete_jadwal, name='delete'),
    path('detail/<int:id>', views.lihat_detail, name='detail')
]