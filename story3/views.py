from django.shortcuts import (render,redirect)
from django.http import HttpResponseRedirect
from .forms import SubForm
from .models import gForm
from . import models

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, "newindex.html")

def contacts(request):
    return render(request, "index.html")

def isi_form(request):
    form = SubForm(request.POST or None)
    if form.is_valid() :
        form.save()
    context = {
        'form' : form
    } 
    return render(request, "form.html",context)

def lihat_response(request) :
    posts = gForm.objects.all()
    context = {
        'jadwal' : posts
    }
    return render(request, "response.html",context)

def delete_jadwal(request, id):
    gForm.objects.get(id=id).delete()
    return redirect ('/lihat_response')

def lihat_detail(request, id):
    subjek = gForm.objects.filter(id=id)
    context = {
        'subjek' : subjek
    }
    return render(request, 'detail.html', context)

