from django import forms
from .models import gForm

class SubForm(forms.ModelForm) :
    class Meta :
        model = gForm
        fields = [
            'term',
            'subject',
            'credit',
            'lecturer',
            'classroom',
            'description',
        ]

        widgets = {
                'term':forms.TextInput(attrs={'class':'form-control','placeholder':'Select your term'}),
                'subject':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your subject'}),
                'credit':forms.TextInput(attrs={'class':'form-control','placeholder':'Select your credits'}),
                'lecturer':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your Lecturer'}),
                'classroom':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter your Classroom'}),
                'description':forms.Textarea(attrs={'class':'form-control','placeholder':'Please describe your subject!'}),
            }
