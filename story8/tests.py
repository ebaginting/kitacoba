from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

# Create your tests here.
class Story8TestCase(TestCase):
    def test_url_is_exist(self):
        response = Client().get("/story8")
        self.assertEqual(response.status_code, 200)

    def test_is_template_used(self):
        response = Client().get("/story8")
        self.assertTemplateUsed(response, 'story8/story8.html')