from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'story8/story8.html')

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)

    data = json.loads(ret.content)
    # data = {'coba': 'ini isinya', 'sesuatu':'asdfsdf'}
    return JsonResponse(data, safe=False)